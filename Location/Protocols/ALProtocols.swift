//
//  ALProtocols.swift
//  Location
//
//  Created by Линник Александр on 30.10.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

import UIKit
import CoreLocation


protocol ALLocationManagerDelegate: class {
    func currentLocation (location : CLLocation)
}

