//
//  ALArea.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit

class ALArea: ALBaseModel {
    
    var name = ""
    var provinceName = ""
    var provinceId: Int64 = 0
    var Id: Int64 = 0
    var lat: Double = 0
    var lng: Double = 0

    
    init(dictionary:NSDictionary){
        if let name = dictionary["name"] as? String {
            self.name = name
        }
        if let province = dictionary["province"] as? NSDictionary {
            if let name = province["name"] as? String {
                self.provinceName = name
            }
            if let provinceId = province["id"] as? NSNumber {
                self.provinceId = provinceId.int64Value
            }
        }
        if let Id = dictionary["id"] as? NSNumber {
            self.Id = Id.int64Value
        }
        if let lat = dictionary["lat"] as? NSNumber {
            self.lat = lat.doubleValue
        }
        if let lng = dictionary["lng"] as? NSNumber {
            self.lng = lng.doubleValue
        }
    }


}
