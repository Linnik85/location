//
//  ALAddress.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit

class ALAddress: ALBaseModel {
    
    var preview = ""
    var block = ""
    var province = ""
    var street = ""
    var area = ""

    init(dictionary:NSDictionary){
        if let preview = dictionary["preview"] as? String {
            self.preview = preview
        }
        if let block = dictionary["block"] as? String {
            self.block = block
        }
        if let province = dictionary["province"] as? String {
            self.province = province
        }
        if let street = dictionary["street"] as? String {
            self.street = street
        }
        if let area = dictionary["area"] as? String {
            self.area = area
        }
    }
}


