//
//  ViewController.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit

class ALBaseViewController: UIViewController {
    
    lazy var router: ALRouter? = {
        let router = ALRouter(withNavigationController: self.navigationController)
        return router
    }()
    
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func _scrollViewContentSize() -> CGSize {
        assert(true, "_scrollViewContentSize should be overriden in subclasses")
        return CGSize.zero
    }
    
    @objc func keyboardWillBeShown(_ aNotification:Notification)  {
        var info = aNotification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        //self.scrollViewForAuth.contentSize = self._scrollViewContentSize()
        self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrame.size.height, 0)
    }
    
    @objc func keyboardWillBeHidden(_ aNotification:Notification)  {
        self.scrollView.contentInset = UIEdgeInsets.zero;
        self._removeObservation()
    }
    
    func _setupObservation()  {
        NotificationCenter.default.addObserver(self, selector: #selector(ALBaseViewController.keyboardWillBeShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ALBaseViewController.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func _removeObservation()  {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func showAlert(_ message:String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: "Ok", style: .default) { (action) in
        }
        alertController.addAction(reloadAction)
        self.present(alertController, animated: true){
        }
    }

}

