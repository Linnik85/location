//
//  ALLocationViewController.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class ALLocationViewController: ALBaseViewController, ALLocationManagerDelegate {
    
    // MARK: - Properties -

    @IBOutlet weak var mapView: GMSMapView!
    var selfMarker : GMSMarker?
    
    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        self._initialSetup()
        
    }
    
    // MARK: - Private -
    
    private func _initialSetup() {
        ALLocationManager.sharedLocationManager.delegate = self
        ALLocationManager.sharedLocationManager.getCurrentLocation()
    }
    
    private func _setupDefaultLocation()  {
        self.currentLocation(location: CLLocation(latitude: 29.368354, longitude: 47.974667))
    }
    
    // MARK: - ALLocationManagerDelegate -
    
    func currentLocation (location : CLLocation) {
        if let selfMarker = self.selfMarker {
            selfMarker.position = location.coordinate
        } else {
            let marker = GMSMarker()
            marker.position = location.coordinate
            marker.icon = UIImage(named: "pinImage")
            marker.map = mapView
            marker.isDraggable = true
            self.selfMarker = marker
        }
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15);
        self.mapView.camera = camera
    }
    
    // MARK: - Actions -
    
    @IBAction func confirm(_ sender: Any) {
        guard let marker = self.selfMarker else {
            return
        }
        self.showLoading()
        ALLocationService.locationService.getAdresses(marker.position) {[weak self] (result, error) in
            self?.hideLoading()
            if let error = error {
                self?.showAlert(error.localizedDescription)
                self?._setupDefaultLocation()
            } else {
                if let address = result as? ALAddress {
                    self?.router?.showAddNewAddressViewController(address, coordinate: marker.position)
                }
            }
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        //do nothing
    }

}
