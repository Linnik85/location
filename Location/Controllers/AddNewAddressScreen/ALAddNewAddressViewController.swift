//
//  ALAddNewAddressViewController.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import SkyFloatingLabelTextField
import DropDown

class ALAddNewAddressViewController: ALBaseViewController  {
    
    // MARK: - Constant -
    
    let kButtonOffset : CGFloat = 20
    
    // MARK: - Properties -
    
    var selectedAddress : ALAddress?
    var selectedArea : ALArea?
    var coordinate : CLLocationCoordinate2D? = nil
    var selfMarker : GMSMarker?
    var areas = [ALArea]()
    
    @IBOutlet weak var addressNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var areaTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var apartmentTypeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var blockTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var streetTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var buildingTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var floorTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var apartmentNoTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var specialInstructionsTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var saveAddressButton: UIButton!
    
    lazy var dropDown: DropDown? = {
        let dropDown = DropDown()
        return dropDown
    }()
    
    // MARK: - Lifecycle -

    override func viewDidLoad() {
        super.viewDidLoad()
        self._initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self._setupCurrentLocation()
        self._setupFieldsWithAdress()
        self._setDelegates()
    }
    
    // MARK: - Private -

    private func _initialSetup() {
        self.fetchAreas()
        self._setupCustomUI()
    }
    
    func fetchAreas() {
        ALLocationService.locationService.getAreas {[weak self] (result, error) in
            if let error = error  {
                self?.showAlert(error.localizedDescription)
            } else {
                self?.hideLoading()
                if let data = result as? NSArray {
                    self?.areas = data as! [ALArea]
                    self?._setupDropDownMenu()
                }
            }
        }
    }
    
    private func _setupCustomUI() {
        self._customizeBackButton()
        self.scrollView.contentSize = self._scrollViewContentSize()
        self.addressNameTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.areaTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.apartmentTypeTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.blockTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.streetTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.buildingTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.apartmentNoTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.mobileNumberTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.specialInstructionsTextField.titleFont = UIFont.systemFont(ofSize: 10)
        self.floorTextField.titleFont = UIFont.systemFont(ofSize: 10)
    }
    
    func _setupDropDownMenu()  {
        self.dropDown?.anchorView = self.areaTextField
        var areaNames = [String]()
        
        for area in self.areas {
            areaNames.append(area.name)
        }
        
        self.dropDown?.dataSource = areaNames
        self.dropDown?.selectionAction = { [unowned self] (index: Int, item: String) in
            self.selectedArea = self.areas[index]
            self.areaTextField.text = item
            self.dropDown?.hide()
        }
    }
    
    private func _setDelegates() {
        self.addressNameTextField.delegate = self
        self.areaTextField.delegate = self
        self.apartmentTypeTextField.delegate = self
        self.blockTextField.delegate = self
        self.streetTextField.delegate = self
        self.buildingTextField.delegate = self
        self.apartmentNoTextField.delegate = self
        self.mobileNumberTextField.delegate = self
        self.specialInstructionsTextField.delegate = self
        self.floorTextField.delegate = self
    }
    
    private func _customizeBackButton() {
        let backButton = UIButton(type: .custom)
        backButton.backgroundColor = UIColor.clear
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.contentHorizontalAlignment = .left
        backButton.setImage(UIImage(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(ALAddNewAddressViewController.backAction), for: .touchUpInside)
        let backItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backItem;
    }
    
    private func _setupCurrentLocation () {
        if let coordinate = self.coordinate {
            let marker = GMSMarker()
            marker.position = coordinate
            marker.icon = UIImage(named: "pinImage")
            marker.map = mapView
            self.selfMarker = marker
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15);
            self.mapView.camera = camera
        }
    }
    
    private func _setupFieldsWithAdress()  {
        if let selectedAddress = self.selectedAddress {
            self.addressNameTextField.text = selectedAddress.preview
            self.blockTextField.text = selectedAddress.block
            self.streetTextField.text = selectedAddress.street
        }
    }
    
    override func _scrollViewContentSize() -> CGSize {
        let size = CGSize(width: UIScreen.main.bounds.size.width, height: self.saveAddressButton.frame.origin.y + self.saveAddressButton.frame.size.height + self.kButtonOffset)
        return size
    }
    
    // MARK: - Actions -

    @IBAction func saveAddress(_ sender: Any) {
        //do nothing
    }
    
    @objc func backAction()  {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITextFieldDelegate

extension ALAddNewAddressViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.areaTextField {
            self.view.endEditing(true)
            self.dropDown?.show()
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self._setupObservation()
    }
}


