//
//  ALResponseHelper.swift
//  Location
//
//  Created by Линник Александр on 03.03.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

import UIKit

class ALResponseHelper: NSObject {
    
    class func errorFromResult (_ result:NSDictionary?) -> NSError? {
        if let errorMessage = result?["error"] as? NSString {
            let error = NSError(domain: "HttpResponseErrorDomain", code: 401, userInfo:[ NSLocalizedDescriptionKey : errorMessage ])
            return error
         }
        return nil
    }
    
    class func getAddressFromSearchResult (_ result:NSDictionary?) -> ALAddress {
        var address = ALAddress(dictionary:[:])
        if let data = result?["address"] as? NSDictionary {
           address = ALAddress(dictionary: data)
        }
        return address
    }
    
    class func getAreasResult(_ result:NSDictionary?) -> Array<ALArea> {
        var areasArray = [ALArea]()
        var areasArrayDict = [NSDictionary]()
        if let data = result?["areas"] as? NSArray {
            if let  areas = data as? [NSDictionary] {
                areasArrayDict = areas
            }
        }
        for areaDict in areasArrayDict {
            let area = ALArea(dictionary: areaDict)
            areasArray.append(area)
        }
        return areasArray
    }

}
