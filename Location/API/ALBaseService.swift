//
//  ALBaseService.swift
//  Location
//
//  Created by Линник Александр on 20.02.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit

class ALBaseService: NSObject {
    
    lazy var apiManager: ALApiManager? = {
        let apiManager = ALApiManager.sharedApiManager
        return apiManager
    }()

}
