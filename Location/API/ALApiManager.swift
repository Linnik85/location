//
//  ALApiManager.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit
import Alamofire

typealias responseBlock = (_ responseObject :Any?, _ error:NSError? , _ status : NSInteger) -> Void
typealias objectBlock = (_ result :Any?, _ error:NSError?) -> Void
    
    
class ALApiManager: NSObject {
        
    static let sharedApiManager = ALApiManager()
    var sessionManager = Alamofire.SessionManager.default
        
        
    func GET(_ URLString:String, parameters:Any, completion:@escaping responseBlock){
      
        self.sessionManager.request(Constants.Host.BaseURL+URLString, method: HTTPMethod.get, parameters: parameters as? [String : Any]).responseJSON{
                response in
                
            if let JSON = response.result.value {
                self._callBlockWithResponse(completion, responseObject: JSON, status: (response.response?.statusCode)!)
            } else {
                self._callBlockWithError(completion, error: response.result.error! as NSError, status:0)
            }
        }
    }
        
    fileprivate func _callBlockWithResponse(_ block:responseBlock, responseObject: Any, status : NSInteger){
        let error = ALResponseHelper.errorFromResult(responseObject as? NSDictionary)
            block(responseObject, error, status)
    }
        
    fileprivate func _callBlockWithError(_ block:responseBlock, error: NSError, status : NSInteger){
        block(nil, error, status)
    }
}
