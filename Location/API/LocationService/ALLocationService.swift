//
//  ALLocationService.swift
//  Location
//
//  Created by Линник Александр on 01.12.17.
//  Copyright © 2017 Alex Linnik. All rights reserved.
//

import UIKit
import CoreLocation

class ALLocationService: ALBaseService {
    static let locationService = ALLocationService()
    
    // MARK: - Requests -
    
    func getAdresses(_ coordinate : CLLocationCoordinate2D, complition: @escaping objectBlock) {
        self.apiManager?.GET("addresses/geolocate", parameters: ["lat": coordinate.latitude,"lng": coordinate.longitude], completion: { (responseObject, error, stats) -> Void in
            self.callBlockWithGeolocateResponse(responseObject, error: error, block: complition)
        })
    }
    
    func getAreas(_ complition: @escaping objectBlock) {
        self.apiManager?.GET("areas", parameters: [:], completion: { (responseObject, error, stats) -> Void in
            self.callBlockWithAreasResponse(responseObject, error: error, block: complition)
        })
    }
    // MARK: - Blocks -

    func callBlockWithGeolocateResponse(_ responseObject:Any?, error : NSError?, block : objectBlock) {
        if (error != nil) {
            block([], error)
        }
        
        if (responseObject != nil) {
            let address = ALResponseHelper.getAddressFromSearchResult(responseObject as? NSDictionary)
            block(address, error)
        }
    }
    
    func callBlockWithAreasResponse(_ responseObject:Any?, error : NSError?, block : objectBlock) {
        if (error != nil) {
            block([], error)
        }
        if (responseObject != nil) {
            let areas = ALResponseHelper.getAreasResult(responseObject as? NSDictionary)
            block(areas, error)
        }
    }
}
