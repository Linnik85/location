//
//  ALLocationManager.swift
//  Location
//
//  Created by Линник Александр on 04.11.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

import UIKit
import CoreLocation


class ALLocationManager: NSObject, CLLocationManagerDelegate {
    
    // MARK: - Properties -
    
    let manager = CLLocationManager()
    static let sharedLocationManager = ALLocationManager()
    
    weak var delegate: ALLocationManagerDelegate?

    // MARK: - Lifecycle -
    
    override init() {
        super.init()
        manager.delegate = self
        manager.distanceFilter=10.0;
        manager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters
    }
    
    // MARK: - Private -
    
    func getCurrentLocation() {
        self.manager.requestAlwaysAuthorization()
        self.manager.requestLocation()
    }
    
    func requestAuthorization()  {
        self.manager.requestAlwaysAuthorization()
    }
    
    // MARK: - CLLocationManagerDelegate -
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first, location.horizontalAccuracy >= 0 else { return }
        if let location = locations.first {
            self.delegate?.currentLocation(location: location)
        }
        self.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
}
