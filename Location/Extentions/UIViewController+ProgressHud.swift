//
//  UIViewController+ProgressHud.swift
//  Location
//
//  Created by Линник Александр on 06.03.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    
    func showLoading() {
        SVProgressHUD.setBackgroundColor(UIColor.black)
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.gradient)
        SVProgressHUD.setRingThickness(5.0)
        SVProgressHUD.show()
    }
    
    func hideLoading() {
    SVProgressHUD.dismiss()
    }
    
    func showSuccesWithStatus(_ status:String) {
        SVProgressHUD.showSuccess(withStatus: status)

    }
}
