//
//  ALRouter.swift
//  Location
//
//  Created by Линник Александр on 29.02.16.
//  Copyright © 2016 Alex Linnik. All rights reserved.
//

import UIKit
import CoreLocation

class ALRouter: NSObject {
    
    // MARK: - Properties -
    
    let summaryViewControllerTopOffset : CGFloat = 162
    
    enum ALStoryboardType : Int {
        case home = 0
    }
    
    weak var navigationController : UINavigationController?

    init(withNavigationController navigationController:UINavigationController?) {
        self.navigationController = navigationController
    }
    
    // MARK: - Public -
    
    func visibleViewController() -> UIViewController? {
        return self.navigationController?.visibleViewController
    }
    
    func showAddNewAddressViewController(_ address :ALAddress, coordinate : CLLocationCoordinate2D) {
        let addNewAddressViewController = self._viewControllerWithClass(String(describing: ALAddNewAddressViewController.self), storyboardType: ALStoryboardType.home) as! ALAddNewAddressViewController
        addNewAddressViewController.selectedAddress = address
        addNewAddressViewController.coordinate = coordinate
        self.navigationController?.pushViewController(addNewAddressViewController, animated: true)
    }
    
    // MARK: - Private -
    
    fileprivate func _storyboardWithType (_ type : ALStoryboardType) -> UIStoryboard {
        let storyboardNames = ["Main"]
        let storyboardName = storyboardNames[type.rawValue]
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard
    }
    
     func _viewControllerWithClass (_ className: String, storyboardType : ALStoryboardType) -> UIViewController {
         let storyboard = self._storyboardWithType(storyboardType)
         let viewController = storyboard.instantiateViewController(withIdentifier: className)
        return viewController
    }
    
    fileprivate func _navigationControllerWithID (_ identifier: String, storyboardType : ALStoryboardType) -> UINavigationController {
        print(identifier)

        let storyboard = self._storyboardWithType(storyboardType)
        let navigationController = storyboard.instantiateViewController(withIdentifier: identifier) as! UINavigationController
        return navigationController
    }

}



